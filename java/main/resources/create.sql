create table cozinha (id bigint not null auto_increment, nome varchar(255), primary key (id)) engine=InnoDB
create table restaurante (id bigint not null auto_increment, nome varchar(255), taxa_frete decimal(19,2), cozinha_id bigint, primary key (id)) engine=InnoDB
alter table restaurante add constraint FK76grk4roudh659skcgbnanthi foreign key (cozinha_id) references cozinha (id)
insert into cozinha (nome)
values ('Brasileira'),
       ('Italiana'),
       ('Mexicana')
insert into restaurante (nome, taxa_frete, cozinha_id)
values ('Sabor do Brasil', 10.5, 1),
       ('Feijoada & Companhia', 12.0, 1),
       ('Tempero do Norte', 8.5, 1),
       ('Café Brasileiro', 15.0, 1),
       ('Churrasco Real', 20.0, 1)
insert into restaurante (nome, taxa_frete, cozinha_id)
values ('La Dolce Vita', 11.0, 2),
       ('Pasta Perfeita', 13.5, 2),
       ('Trattoria Bella Notte', 14.0, 2),
       ('Pizza Paradiso', 9.0, 2),
       ('Ristorante Amore', 18.0, 2)
insert into restaurante (nome, taxa_frete, cozinha_id)
values ('Fiesta Mexicana', 7.0, 3),
       ('El Taco Loco', 12.5, 3),
       ('Cantina del Sol', 10.0, 3),
       ('Sabor de México', 13.0, 3),
       ('Casa del Burrito', 16.0, 3)

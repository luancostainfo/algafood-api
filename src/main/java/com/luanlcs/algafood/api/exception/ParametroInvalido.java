package com.luanlcs.algafood.api.exception;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ParametroInvalido {

    private String campo;
    private String mensagem;

}

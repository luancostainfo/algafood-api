create table cozinha
(
    id   bigint not null auto_increment,
    nome varchar(255),
    primary key (id)
) engine = InnoDB;

create table restaurante
(
    id         bigint not null auto_increment,
    nome       varchar(255),
    taxa_frete decimal(19, 2),
    cozinha_id bigint,
    primary key (id)
) engine = InnoDB;

alter table restaurante
    add constraint FK76grk4roudh659skcgbnanthi foreign key (cozinha_id) references cozinha (id);


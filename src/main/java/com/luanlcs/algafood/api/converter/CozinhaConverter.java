package com.luanlcs.algafood.api.converter;

import com.luanlcs.algafood.api.dto.input.CozinhaRequestDTO;
import com.luanlcs.algafood.api.dto.output.CozinhaResponseDTO;
import com.luanlcs.algafood.domain.model.Cozinha;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CozinhaConverter {

    public CozinhaResponseDTO toResponseDTO(Cozinha cozinha) {
        CozinhaResponseDTO cozinhaResponseDTO = new CozinhaResponseDTO();
        cozinhaResponseDTO.setId(cozinha.getId());
        cozinhaResponseDTO.setNome(cozinha.getNome());
        return cozinhaResponseDTO;
    }

    public List<CozinhaResponseDTO> toResponseDTO(List<Cozinha> cozinhas) {
        return cozinhas.stream().map(this::toResponseDTO).collect(Collectors.toList());
    }

    public Cozinha toEntity(CozinhaRequestDTO cozinhaRequestDTO) {
        Cozinha cozinha = new Cozinha();
        cozinha.setNome(cozinhaRequestDTO.getNome());
        return cozinha;
    }

    public Page<CozinhaResponseDTO> toResponseDTO(Page<Cozinha> cozinhas) {
        return cozinhas.map(this::toResponseDTO);
    }
}

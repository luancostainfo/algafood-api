package com.luanlcs.algafood.api.controller;

import com.luanlcs.algafood.api.converter.CozinhaConverter;
import com.luanlcs.algafood.api.dto.filter.CozinhaFilterDTO;
import com.luanlcs.algafood.api.dto.input.CozinhaRequestDTO;
import com.luanlcs.algafood.api.dto.output.CozinhaResponseDTO;
import com.luanlcs.algafood.domain.service.CozinhaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/cozinhas")
public class CozinhaController {

    private final CozinhaService cozinhaService;
    private final CozinhaConverter cozinhaConverter;

    @GetMapping("/{id}")
    public CozinhaResponseDTO buscarPorId(@PathVariable Long id) {
        return cozinhaConverter.toResponseDTO(cozinhaService.buscarPorId(id));
    }

    @GetMapping
    public List<CozinhaResponseDTO> listar() {
        return cozinhaConverter.toResponseDTO(cozinhaService.listar());
    }

    @GetMapping("/filtros")
    public Page<CozinhaResponseDTO> pesquisar(CozinhaFilterDTO cozinhaFilterDTO, Pageable pageable) {
        return cozinhaConverter.toResponseDTO(cozinhaService.pesquisar(cozinhaFilterDTO, pageable));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public CozinhaResponseDTO cadastrar(@RequestBody @Valid CozinhaRequestDTO cozinhaRequestDTO) {
        return cozinhaConverter.toResponseDTO(cozinhaService.cadastrar(cozinhaConverter.toEntity(cozinhaRequestDTO)));
    }

    @PutMapping("/{id}")
    public CozinhaResponseDTO alterar(@PathVariable Long id, @RequestBody @Valid CozinhaRequestDTO cozinhaRequestDTO) {
        return cozinhaConverter.toResponseDTO(cozinhaService.alterar(id, cozinhaConverter.toEntity(cozinhaRequestDTO)));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void excluir(@PathVariable Long id) {
        cozinhaService.excluir(id);
    }

}

package com.luanlcs.algafood.api.dto.filter;

import lombok.Data;

@Data
public class CozinhaFilterDTO {
    private String nome;
}

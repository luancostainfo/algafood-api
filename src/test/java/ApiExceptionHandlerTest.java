package com.luanlcs.algafood.api.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.luanlcs.algafood.domain.exception.RecursoNaoEncontradoException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ApiExceptionHandlerTest {

    private ApiExceptionHandler apiExceptionHandler;
    private WebRequest request;

    @BeforeEach
    public void setUp() {
        apiExceptionHandler = new ApiExceptionHandler();
        request = mock(WebRequest.class);
    }

    @Test
    public void testHandleResourceNotFound() {
        RecursoNaoEncontradoException ex = new RecursoNaoEncontradoException("Recurso não encontrado");

        ResponseEntity<Object> response = apiExceptionHandler.handleResourceNotFound(ex, request);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        ProblemDetails body = (ProblemDetails) response.getBody();
        assertNotNull(body);
        assertEquals("Recurso não encontrado", body.getTitulo());
        assertEquals("Recurso não encontrado", body.getDetalhe());
    }

    @Test
    public void testHandleInvalidFormatException() {
        InvalidFormatException ex = mock(InvalidFormatException.class);
        when(ex.getValue()).thenReturn("valor");
        when(ex.getTargetType()).thenReturn((Class) Integer.class);

        ResponseEntity<Object> response = apiExceptionHandler.handleInvalidFormatException(ex, new HttpHeaders(), HttpStatus.BAD_REQUEST,
                request);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        ProblemDetails body = (ProblemDetails) response.getBody();
        assertNotNull(body);
        assertEquals("Formato inválido", body.getTitulo());
        assertTrue(body.getDetalhe().contains("valor"));
    }

    @Test
    public void testHandleGeneralException() {
        Exception ex = new Exception("Erro genérico");

        ResponseEntity<Object> response = apiExceptionHandler.handleGeneralException(ex, request);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        ProblemDetails body = (ProblemDetails) response.getBody();
        assertNotNull(body);
        assertEquals("Erro inesperado", body.getTitulo());
        assertEquals("Ocorreu um erro no sistema. Tente novamente mais tarde.", body.getDetalhe());
    }
}

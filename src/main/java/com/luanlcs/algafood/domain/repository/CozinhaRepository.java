package com.luanlcs.algafood.domain.repository;

import com.luanlcs.algafood.domain.model.Cozinha;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CozinhaRepository extends JpaRepository<Cozinha, Long>, JpaSpecificationExecutor<Cozinha> {
}

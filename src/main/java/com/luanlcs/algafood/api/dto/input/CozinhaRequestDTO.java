package com.luanlcs.algafood.api.dto.input;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class CozinhaRequestDTO {

    @NotBlank
    @Size(min = 3, max = 100)
    private String nome;

}

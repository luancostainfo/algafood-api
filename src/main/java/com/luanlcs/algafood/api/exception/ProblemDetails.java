package com.luanlcs.algafood.api.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
public class ProblemDetails {

    private Integer status;
    @Builder.Default
    private LocalDateTime timestamp = LocalDateTime.now();
    private String titulo;
    private String detalhe;
    private List<ParametroInvalido> parametrosInvalidos;


    public void setParametrosInvalidos(List<ParametroInvalido> parametrosInvalidos) {
        this.parametrosInvalidos = parametrosInvalidos;
    }
}

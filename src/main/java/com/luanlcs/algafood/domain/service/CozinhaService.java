package com.luanlcs.algafood.domain.service;

import com.luanlcs.algafood.api.dto.filter.CozinhaFilterDTO;
import com.luanlcs.algafood.domain.exception.RecursoNaoEncontradoException;
import com.luanlcs.algafood.domain.model.Cozinha;
import com.luanlcs.algafood.domain.repository.CozinhaRepository;
import com.luanlcs.algafood.domain.repository.CozinhaSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CozinhaService {

    private final CozinhaRepository cozinhaRepository;
    private static final String MSG_RECURSO_NAO_ENCONTRADO = "Cozinha de id %s não encontrada.";

    public Cozinha buscarPorId(Long id) {
        return cozinhaRepository.findById(id).orElseThrow(() ->
                new RecursoNaoEncontradoException(String.format(MSG_RECURSO_NAO_ENCONTRADO, id)));
    }

    public List<Cozinha> listar() {
        return cozinhaRepository.findAll();
    }

    @Transactional
    public Cozinha cadastrar(Cozinha cozinha) {
        return cozinhaRepository.save(cozinha);
    }

    @Transactional
    public Cozinha alterar(Long id, Cozinha cozinha) {
        Cozinha cozinhaPorId = buscarPorId(id);
        BeanUtils.copyProperties(cozinha, cozinhaPorId, "id");
        return cozinhaPorId;
    }

    @Transactional
    public void excluir(Long id) {
        cozinhaRepository.delete(buscarPorId(id));
    }

    public Page<Cozinha> pesquisar(CozinhaFilterDTO cozinhaFilterDTO, Pageable pageable) {
        return cozinhaRepository.findAll(CozinhaSpecification.pesquisar(cozinhaFilterDTO), pageable);
    }
}

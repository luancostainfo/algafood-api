
package com.luanlcs.algafood.api.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.PropertyBindingException;
import com.luanlcs.algafood.domain.exception.RecursoNaoEncontradoException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(RecursoNaoEncontradoException.class)
    public ResponseEntity<Object> handleResourceNotFound(RecursoNaoEncontradoException ex, WebRequest request) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        ProblemDetails problem = createProblemDetail(status, "Recurso não encontrado", ex.getMessage());
        return handleExceptionInternal(ex, problem, new HttpHeaders(), status, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<ParametroInvalido> invalidParameters = getParametroInvalidos(ex);
        ProblemDetails problem = createProblemDetail(status, "Parâmetros inválidos", "Alguns parâmetros estão incorretos.");
        problem.setParametrosInvalidos(invalidParameters);  // Mantendo a lista de parâmetros inválidos
        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (ex.getCause() instanceof InvalidFormatException) {
            return handleInvalidFormatException((InvalidFormatException) ex.getCause(), headers, status, request);
        } else if (ex.getCause() instanceof PropertyBindingException) {
            return handlePropertyBindingException((PropertyBindingException) ex.getCause(), headers, status, request);
        }

        ProblemDetails problem = createProblemDetail(status, "Corpo da requisição inválido", "O corpo da requisição está malformado ou contém erros.");
        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    private ResponseEntity<Object> handlePropertyBindingException(PropertyBindingException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String path = getJsonPath(ex);
        String detalhe = String.format("A propriedade '%s' não existe. Verifique o JSON e remova essa propriedade.",
                path);
        ProblemDetails problem = createProblemDetail(status, "Propriedade desconhecida", detalhe);
        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    public ResponseEntity<Object> handleInvalidFormatException(InvalidFormatException ex, HttpHeaders headers,
                                                          HttpStatus status, WebRequest request) {
        String path = getJsonPath(ex);
        String detalhe = String.format("A propriedade '%s' recebeu um valor do tipo errado. Informe um valor " +
                "compatível" +
                " " +
                "com" +
                " o tipo '%s'.", path, ex.getTargetType().getSimpleName());
        ProblemDetails problem = createProblemDetail(status, "Formato inválido", detalhe);
        return handleExceptionInternal(ex, problem, headers, status, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleGeneralException(Exception ex, WebRequest request) {
        ProblemDetails problem = createProblemDetail(HttpStatus.INTERNAL_SERVER_ERROR, "Erro inesperado", "Ocorreu um erro no sistema. Tente novamente mais tarde.");
        return handleExceptionInternal(ex, problem, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    // Métodos de suporte
    private ProblemDetails createProblemDetail(HttpStatus status, String titulo, String detalhe) {
        return ProblemDetails.builder()
                .status(status.value())
                .titulo(titulo)
                .detalhe(detalhe)
                .build();
    }

    private String getJsonPath(JsonMappingException ex) {
        return ex.getPath().stream()
                .map(JsonMappingException.Reference::getFieldName)
                .collect(Collectors.joining("."));
    }

    private List<ParametroInvalido> getParametroInvalidos(MethodArgumentNotValidException ex) {
        return ex.getBindingResult().getFieldErrors().stream()
                .map(error -> new ParametroInvalido(error.getField(), error.getDefaultMessage()))
                .collect(Collectors.toList());
    }
}

set foreign_key_checks = 0;

truncate cozinha;
truncate restaurante;


insert into cozinha (nome)
values ('Brasileira'),
       ('Italiana'),
       ('Mexicana');

-- Inserir restaurantes com cozinha Brasileira
insert into restaurante (nome, taxa_frete, cozinha_id)
values ('Sabor do Brasil', 10.5, 1),
       ('Feijoada & Companhia', 12.0, 1),
       ('Tempero do Norte', 8.5, 1),
       ('Café Brasileiro', 15.0, 1),
       ('Churrasco Real', 20.0, 1);

-- Inserir restaurantes de cozinha Italiana
insert into restaurante (nome, taxa_frete, cozinha_id)
values ('La Dolce Vita', 11.0, 2),
       ('Pasta Perfeita', 13.5, 2),
       ('Trattoria Bella Notte', 14.0, 2),
       ('Pizza Paradiso', 9.0, 2),
       ('Ristorante Amore', 18.0, 2);

-- Inserir restaurantes de cozinha Mexicana
insert into restaurante (nome, taxa_frete, cozinha_id)
values ('Fiesta Mexicana', 7.0, 3),
       ('El Taco Loco', 12.5, 3),
       ('Cantina del Sol', 10.0, 3),
       ('Sabor de México', 13.0, 3),
       ('Casa del Burrito', 16.0, 3);

set foreign_key_checks = 1;
commit;
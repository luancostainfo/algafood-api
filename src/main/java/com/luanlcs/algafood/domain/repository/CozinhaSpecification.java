package com.luanlcs.algafood.domain.repository;

import com.luanlcs.algafood.api.dto.filter.CozinhaFilterDTO;
import com.luanlcs.algafood.domain.model.Cozinha;
import org.springframework.data.jpa.domain.Specification;

public class CozinhaSpecification {

    public static Specification<Cozinha> comNome(String nome) {
        return (root, query, criteriaBuilder) -> nome == null
                ? criteriaBuilder.conjunction()
                : criteriaBuilder.equal(root.get("nome"), nome);
    }

    public static Specification<Cozinha> pesquisar(CozinhaFilterDTO cozinhaFilterDTO) {
        return comNome(cozinhaFilterDTO.getNome());
    }


}

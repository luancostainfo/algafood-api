package com.luanlcs.algafood.api.dto.output;

import lombok.Data;

@Data
public class CozinhaResponseDTO {

    private Long id;
    private String nome;

}
